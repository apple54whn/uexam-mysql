create sequence SEQ_EXAM_PAPER
minvalue 1
maxvalue 9999999999
start with 1000
increment by 1
cache 20
order;

create sequence SEQ_EXAM_PAPER_ANSWER
minvalue 1
maxvalue 9999999999
start with 1000
increment by 1
cache 20
order;

create sequence SEQ_EXAM_PAPER_QUESTION_ANSWER
minvalue 1
maxvalue 9999999999
start with 1000
increment by 1
cache 20
order;

create sequence SEQ_MESSAGE
minvalue 1
maxvalue 9999999999
start with 1000
increment by 1
cache 20
order;

create sequence SEQ_MESSAGE_USER
minvalue 1
maxvalue 9999999999
start with 1000
increment by 1
cache 20
order;


create sequence SEQ_QUESTION
minvalue 1
maxvalue 9999999999
start with 1000
increment by 1
cache 20
order;

create sequence SEQ_SUBJECT
minvalue 1
maxvalue 9999999999
start with 1000
increment by 1
cache 20
order;

create sequence SEQ_TASK_EXAM
minvalue 1
maxvalue 9999999999
start with 1000
increment by 1
cache 20
order;

create sequence SEQ_TASK_EXAM_CUSTOMER_ANSWER
minvalue 1
maxvalue 9999999999
start with 1000
increment by 1
cache 20
order;

create sequence SEQ_TEXT_CONTENT
minvalue 1
maxvalue 9999999999
start with 1000
increment by 1
cache 20
order;

create sequence SEQ_USER
minvalue 1
maxvalue 9999999999
start with 1000
increment by 1
cache 20
order;

create sequence SEQ_USER_EVENT_LOG
minvalue 1
maxvalue 9999999999
start with 1000
increment by 1
cache 20
order;

create sequence SEQ_USER_TOKEN
minvalue 1
maxvalue 9999999999
start with 1000
increment by 1
cache 20
order;
