-- auto-generated definition
create table t_exam_paper
(
    id                       number(32)   NOT NULL constraint PK_EXAM_PAPER primary key,
    name                  varchar(255) null ,
    subject_id            number(32)           null ,
    paper_type            number(32)           null ,
    grade_level           number(32)           null ,
    score                 number(32)           null ,
    question_count        number(32)           null ,
    suggest_time          number(32)           null ,
    limit_start_time      timestamp(6)      null ,
    limit_end_time        timestamp(6)      null ,
    frame_text_content_id number(32)           null ,
    create_user           number(32)           null,
    create_time           timestamp(6)      null,
    deleted               number(1)          null,
    task_exam_id          number(32)          null
);

-- auto-generated definition
create table t_exam_paper_answer
(
    id                       number(32)   NOT NULL constraint PK_EXAM_PAPER_ANSWER primary key,
    exam_paper_id    number(32)          null,
    paper_name       varchar2(255) null ,
    paper_type       number(32)          null ,
    subject_id       number(32)          null ,
    system_score     number(32)          null ,
    user_score       number(32)          null ,
    paper_score      number(32)          null ,
    question_correct number(32)          null ,
    question_count   number(32)          null ,
    do_time          number(32)          null ,
    status           number(32)          null ,
    create_user      number(32)          null ,
    create_time      timestamp(6)     null ,
    task_exam_id     number(32)          null
);



CREATE TABLE T_EXAM_PAPER_QUESTION_ANSWER
(
    id                       number(32)   NOT NULL constraint PK_EXAM_PAPER_QUESTION_ANSWER primary key,
    question_id              number(32)   NULL,
    exam_paper_id            number(32)   NULL,
    exam_paper_answer_id     number(32)   NULL,
    question_type            number(32)   NULL,
    subject_id               number(32)   NULL,
    customer_score           number(32)   NULL,
    question_score           number(32)   NULL,
    question_text_content_id number(32)   NULL,
    answer                   varchar2(255),
    text_content_id          number(32)   NULL,
    do_right                 number(1)    NULL,
    create_user              number(32)   NULL,
    create_time              timestamp(6) NULL,
    item_order               number(32)   NULL
);


CREATE TABLE t_message
(
    id                 number(32)   NOT NULL constraint PK_MESSAGE primary key,
    title              varchar2(255),
    content            varchar2(500),
    create_time        timestamp(6) NULL,
    send_user_id       number(32)   NULL,
    send_user_name     varchar2(255),
    send_real_name     varchar2(255),
    receive_user_count number(32)   NULL,
    read_count         number(32)   NULL
);

CREATE TABLE t_message_user
(
    id                 number(32)   NOT NULL constraint PK_MESSAGE_USER primary key,
    message_id        number(32)   NULL,
    receive_user_id   number(32)   NULL,
    receive_user_name varchar2(255),
    receive_real_name varchar2(255),
    readed            number(1)    NULL,
    create_time       timestamp(6) NULL,
    read_time         timestamp(6) NULL
);

create table t_question
(
    id                 number(32)   NOT NULL constraint PK_QUESTION primary key,

    question_type        number(32)    null,
    subject_id           number(32)    null,
    score                number(32)    null,
    grade_level          number(32)    null,
    difficult            number(32)    null,
    correct              varchar2(255) null,
    info_text_content_id number(32)    null,
    create_user          number(32)    null,
    status               number(32)    null,
    create_time          timestamp(6)  null,
    deleted              number(1)     null
);


CREATE TABLE t_subject
(
    id                 number(32)   NOT NULL constraint PK_SUBJECT primary key,
    name       varchar2(255) null,
    level_id      number(32) NULL,
    level_name varchar2(255),
    item_order number(32) NULL,
    deleted    number(1)  NULL
);

CREATE TABLE t_task_exam
(
    id                 number(32)   NOT NULL constraint PK_TASK_EXAM primary key,
    title                 varchar2(255),
    grade_level           number(32)   NULL,
    frame_text_content_id number(32)   NULL,
    create_user           number(32)   NULL,
    create_time           timestamp(6) NULL,
    deleted               number(1)    NULL,
    create_user_name      varchar2(255)
);


CREATE TABLE t_task_exam_customer_answer
(
    id                 number(32)   NOT NULL constraint PK_TASK_EXAM_CUSTOMER_ANSWER primary key,
    task_exam_id    number(32)   NULL,
    create_user     number(32)  NULL,
    create_time     timestamp(6) NULL,
    text_content_id number(32)   NULL
);


CREATE TABLE t_user
(
    id                 number(32)   NOT NULL constraint PK_USER primary key,
    user_uuid        varchar2(36),
    user_name        varchar2(255),
    password         varchar2(255),
    real_name        varchar2(255),
    age              number(32)   NULL,
    sex              number(32)   NULL,
    birth_day        timestamp(6) NULL,
    user_level       number(32)   NULL,
    phone            varchar2(255),
    role             number(32)   NULL,
    status           number(32)   NULL,
    image_path       varchar2(255),
    create_time      timestamp(6) NULL,
    modify_time      timestamp(6) NULL,
    last_active_time timestamp(6) NULL,
    deleted          number(1)    NULL,
    wx_open_id       varchar2(255)
);


CREATE TABLE t_user_event_log
(
    id                 number(32)  NOT NULL constraint PK_USER_EVENT_LOG primary key,
    user_id     number(32)   NULL,
    user_name   varchar2(255),
    real_name   varchar2(255),
    content     clob,
    create_time timestamp(6) NULL
);


CREATE TABLE t_user_token
(
    id                 number(32)   NOT NULL constraint PK_USER_TOKEN primary key,
    token       varchar2(36),
    user_id     number(32)   NULL,
    wx_open_id  varchar2(255),
    create_time timestamp(6) NULL,
    end_time    timestamp(6) NULL,
    user_name   varchar2(255)
);


CREATE TABLE t_text_content
(
    id                 number(32)   NOT NULL constraint PK_TEXT_CONTENT primary key,
    content  clob constraint ENSURE_JSON_TEXT_CONTENT
            check (content IS JSON),
    create_time timestamp(6) NULL
);


