INSERT INTO t_exam_paper VALUES (1, '固定试卷', 59, 1, 12, 70, 2, 20, NULL, NULL, 3, 2, systimestamp, 0, NULL);
INSERT INTO t_exam_paper VALUES (2, '测试试卷', 68, 1, 1, 150, 2, 60, NULL, NULL, 8, 2, systimestamp, 0, NULL);


INSERT INTO t_exam_paper_answer VALUES (1, 1, '固定试卷', 1, 59, 30, 60, 70, 1, 2, 4, 2, 1, systimestamp, NULL);
INSERT INTO t_exam_paper_answer VALUES (2, 1, '固定试卷', 1, 59, 30, 30, 70, 1, 2, 8, 1, 1, systimestamp, NULL);


INSERT INTO t_exam_paper_question_answer VALUES (1, 1, 1, 1, 1, 59, 30, 30, 1, 'B', NULL, 1, 1, systimestamp, 1);
INSERT INTO t_exam_paper_question_answer VALUES (2, 2, 1, 1, 4, 59, 30, 40, 2, NULL, 4, 0, 1, systimestamp, 2);
INSERT INTO t_exam_paper_question_answer VALUES (3, 1, 1, 2, 1, 59, 30, 30, 1, 'B', NULL, 1, 1, systimestamp, 1);
INSERT INTO t_exam_paper_question_answer VALUES (4, 2, 1, 2, 4, 59, 0, 40, 2, NULL, 6, NULL, 1, systimestamp, 2);


INSERT INTO t_message VALUES (1, '分为非', '分为分为', systimestamp, 2, 'admin', '管理员', 1, 1);



INSERT INTO t_message_user VALUES (1, 1, 1, 'student', '学生', 1, systimestamp, systimestamp);



INSERT INTO t_question VALUES (1, 1, 59, 30, 12, 4, 'B', 1, 2, 1, systimestamp, 0);
INSERT INTO t_question VALUES (2, 4, 59, 40, 12, 4, '', 2, 2, 1, systimestamp, 0);
INSERT INTO t_question VALUES (3, 1, 68, 50, 1, 5, 'C', 5, 2, 1, systimestamp, 1);
INSERT INTO t_question VALUES (4, 1, 68, 100, 1, 5, 'C', 7, 2, 1, systimestamp, 1);
INSERT INTO t_question VALUES (5, 1, 68, 230, 1, 5, 'C', 9, 2, 1, systimestamp, 1);
INSERT INTO t_question VALUES (6, 1, 69, 100, 1, 5, 'C', 10, 2, 1, systimestamp, 1);
INSERT INTO t_question VALUES (7, 1, 1, 1000, 1, 5, 'C', 11, 2, 1, systimestamp, 0);
INSERT INTO t_question VALUES (8, 1, 2, 10000, 1, 5, 'C', 12, 2, 1, systimestamp, 0);


INSERT INTO t_subject VALUES (1, '语文', 1, '一年级', NULL, 0);
INSERT INTO t_subject VALUES (2, '数学', 1, '一年级', NULL, 0);
INSERT INTO t_subject VALUES (3, '语文', 2, '二年级', NULL, 0);
INSERT INTO t_subject VALUES (4, '数学', 2, '二年级', NULL, 0);
INSERT INTO t_subject VALUES (5, '英语', 2, '二年级', NULL, 0);
INSERT INTO t_subject VALUES (6, '英语', 1, '一年级', NULL, 0);
INSERT INTO t_subject VALUES (7, '语文', 3, '三年级', NULL, 0);
INSERT INTO t_subject VALUES (8, '数学', 3, '三年级', NULL, 0);
INSERT INTO t_subject VALUES (9, '英语', 3, '三年级', NULL, 0);
INSERT INTO t_subject VALUES (10, '语文', 4, '四年级', NULL, 0);
INSERT INTO t_subject VALUES (11, '数学', 4, '四年级', NULL, 0);
INSERT INTO t_subject VALUES (12, '英语', 4, '四年级', NULL, 0);
INSERT INTO t_subject VALUES (13, '语文', 5, '五年级', NULL, 0);
INSERT INTO t_subject VALUES (14, '数学', 5, '五年级', NULL, 0);
INSERT INTO t_subject VALUES (15, '英语', 5, '五年级', NULL, 0);
INSERT INTO t_subject VALUES (16, '语文', 6, '六年级', NULL, 0);
INSERT INTO t_subject VALUES (17, '数学', 6, '六年级', NULL, 0);
INSERT INTO t_subject VALUES (18, '英语', 6, '六年级', NULL, 0);
INSERT INTO t_subject VALUES (19, '语文', 7, '初一', NULL, 0);
INSERT INTO t_subject VALUES (20, '数学', 7, '初一', NULL, 0);
INSERT INTO t_subject VALUES (21, '英语', 7, '初一', NULL, 0);
INSERT INTO t_subject VALUES (22, '政治', 7, '初一', NULL, 0);
INSERT INTO t_subject VALUES (23, '历史', 7, '初一', NULL, 0);
INSERT INTO t_subject VALUES (24, '地理', 7, '初一', NULL, 0);
INSERT INTO t_subject VALUES (25, '生物', 7, '初一', NULL, 0);
INSERT INTO t_subject VALUES (26, '语文', 8, '初二', NULL, 0);
INSERT INTO t_subject VALUES (27, '数学', 8, '初二', NULL, 0);
INSERT INTO t_subject VALUES (28, '英语', 8, '初二', NULL, 0);
INSERT INTO t_subject VALUES (29, '政治', 8, '初二', NULL, 0);
INSERT INTO t_subject VALUES (30, '历史', 8, '初二', NULL, 0);
INSERT INTO t_subject VALUES (31, '地理', 8, '初二', NULL, 0);
INSERT INTO t_subject VALUES (32, '生物', 8, '初二', NULL, 0);
INSERT INTO t_subject VALUES (33, '物理', 8, '初二', NULL, 0);
INSERT INTO t_subject VALUES (34, '语文', 9, '初三', NULL, 0);
INSERT INTO t_subject VALUES (35, '数学', 9, '初三', NULL, 0);
INSERT INTO t_subject VALUES (36, '英语', 9, '初三', NULL, 0);
INSERT INTO t_subject VALUES (37, '政治', 9, '初三', NULL, 0);
INSERT INTO t_subject VALUES (38, '历史', 9, '初三', NULL, 0);
INSERT INTO t_subject VALUES (39, '物理', 9, '初三', NULL, 0);
INSERT INTO t_subject VALUES (40, '化学', 9, '初三', NULL, 0);
INSERT INTO t_subject VALUES (41, '语文', 10, '高一', NULL, 0);
INSERT INTO t_subject VALUES (42, '数学', 10, '高一', NULL, 0);
INSERT INTO t_subject VALUES (43, '英语', 10, '高一', NULL, 0);
INSERT INTO t_subject VALUES (44, '历史', 10, '高一', NULL, 0);
INSERT INTO t_subject VALUES (45, '政治', 10, '高一', NULL, 0);
INSERT INTO t_subject VALUES (46, '地理', 10, '高一', NULL, 0);
INSERT INTO t_subject VALUES (47, '化学', 10, '高一', NULL, 0);
INSERT INTO t_subject VALUES (48, '物理', 10, '高一', NULL, 0);
INSERT INTO t_subject VALUES (49, '生物', 10, '高一', NULL, 0);
INSERT INTO t_subject VALUES (50, '语文', 11, '高二', NULL, 0);
INSERT INTO t_subject VALUES (51, '数学', 11, '高二', NULL, 0);
INSERT INTO t_subject VALUES (52, '英语', 11, '高二', NULL, 0);
INSERT INTO t_subject VALUES (53, '历史', 11, '高二', NULL, 0);
INSERT INTO t_subject VALUES (54, '政治', 11, '高二', NULL, 0);
INSERT INTO t_subject VALUES (55, '化学', 11, '高二', NULL, 0);
INSERT INTO t_subject VALUES (56, '地理', 11, '高二', NULL, 0);
INSERT INTO t_subject VALUES (57, '物理', 11, '高二', NULL, 0);
INSERT INTO t_subject VALUES (58, '生物', 11, '高二', NULL, 0);
INSERT INTO t_subject VALUES (59, '语文', 12, '高三', NULL, 0);
INSERT INTO t_subject VALUES (60, '数学', 12, '高三', NULL, 0);
INSERT INTO t_subject VALUES (61, '英语', 12, '高三', NULL, 0);
INSERT INTO t_subject VALUES (62, '历史', 12, '高三', NULL, 0);
INSERT INTO t_subject VALUES (63, '政治', 12, '高三', NULL, 0);
INSERT INTO t_subject VALUES (64, '地理', 12, '高三', NULL, 0);
INSERT INTO t_subject VALUES (65, '化学', 12, '高三', NULL, 0);
INSERT INTO t_subject VALUES (66, '物理', 12, '高三', NULL, 0);
INSERT INTO t_subject VALUES (67, '生物', 12, '高三', NULL, 0);
INSERT INTO t_subject VALUES (69, 'Python', 1, '一年级', NULL, 0);



INSERT INTO t_user VALUES (1, 'd2d29da2-dcb3-4013-b874-727626236f47', 'student', 'D1AGFL+Gx37t0NPG4d6biYP5Z31cNbwhK5w1lUeiHB2zagqbk8efYfSjYoh1Z/j1dkiRjHU+b0EpwzCh8IGsksJjzD65ci5LsnodQVf4Uj6D3pwoscXGqmkjjpzvSJbx42swwNTA+QoDU8YLo7JhtbUK2X0qCjFGpd+8eJ5BGvk=', '学生', 17, 1, systimestamp, 12, '15007133738', 1, 1, NULL, systimestamp, NULL, NULL, 0, 'oaWyB4kWcyKeJ441iQcnJXm14Uhg');
INSERT INTO t_user VALUES (2, '52045f5f-a13f-4ccc-93dd-f7ee8270ad4c', 'admin', 'D1AGFL+Gx37t0NPG4d6biYP5Z31cNbwhK5w1lUeiHB2zagqbk8efYfSjYoh1Z/j1dkiRjHU+b0EpwzCh8IGsksJjzD65ci5LsnodQVf4Uj6D3pwoscXGqmkjjpzvSJbx42swwNTA+QoDU8YLo7JhtbUK2X0qCjFGpd+8eJ5BGvk=', '管理员', 30, 1, systimestamp, NULL, NULL, 3, 1, NULL, systimestamp, NULL, NULL, 0, NULL);
INSERT INTO t_user VALUES (3, '52379b13-5c01-43f9-a427-899ae57989c0', 'alvis', 'fnanT3ob78rUADLF8Mky+N29gFyP5952C861ONTSn0A9n8VXU414aSqIVghU+YOVc6UHltxMM+AVymdZPssw+zIUiJtbWu4+zYH4iDfpIHyJW1Tb01IVs3nJPId5YMVWd9rMBBkavA73zOjdwkm13HcV98bFMxyhIqoNuuWdCSY=', 'alvis', NULL, NULL, NULL, 1, NULL, 1, 1, NULL, systimestamp, NULL, systimestamp, 0, NULL);
INSERT INTO t_user VALUES (4, '4e182be9-3ee0-45a4-8329-fd79aee3b8e2', '32', 'je+NzP95ymeyrTYnFy7+SLqWlePAYvV4AoZBnVhNAJcYYPDJ9hpLMOj0epD7nkz9N1F/IunV4kK8HylCqGZoDFGx4/ey8LKVElG1JYxbZEvAf6OM8Qu/gvL6Hhyan3UHMbYcgXQ16gZtasHrz3K4s3Pb0x14GZtN9WBqqBw4Vec=', '32', NULL, NULL, NULL, NULL, NULL, 2, 2, NULL, systimestamp, NULL, systimestamp, 0, NULL);
INSERT INTO t_user VALUES (5, 'f0cfa3c5-7131-48af-a2be-b8c792018daa', 'bbbvc', 'kc0DETYvKgh9AYyb2oV+b7Z9+DOrcrAbp5F/KMFCtLE5hPyhXy7kHqksNEcCvsPgW2jygkGF6Gfj5iyhRwA/fhacW/LR7+8zOGUQ3hyejPygLW+H2VEkUVnV/LlkD5ge7chqAdBRUkU7uW6MoFGscPL1oTTLusjE5e1hRmHkdWQ=', '2323', NULL, NULL, NULL, NULL, NULL, 3, 1, NULL, systimestamp, NULL, systimestamp, 0, NULL);



INSERT INTO t_user_event_log VALUES (1, 1, 'student', '学生', 'student 登录了学之思考试系统', systimestamp);
INSERT INTO t_user_event_log VALUES (2, 2, 'admin', '管理员', 'admin 登录了学之思考试系统', systimestamp);
INSERT INTO t_user_event_log VALUES (3, 2, 'admin', '管理员', 'admin 登出了学之思考试系统', systimestamp);
INSERT INTO t_user_event_log VALUES (4, 1, 'student', '学生', 'student 登录了学之思考试系统', systimestamp);
INSERT INTO t_user_event_log VALUES (5, 1, 'student', '学生', 'student 提交试卷：固定试卷 得分：3 耗时：4 秒', systimestamp);
INSERT INTO t_user_event_log VALUES (6, 1, 'student', '学生', 'student 批改试卷：固定试卷 得分：6', systimestamp);
INSERT INTO t_user_event_log VALUES (7, 2, 'admin', '管理员', 'admin 登录了学之思考试系统', systimestamp);
INSERT INTO t_user_event_log VALUES (8, 2, 'admin', '管理员', 'admin 登出了学之思考试系统', systimestamp);
INSERT INTO t_user_event_log VALUES (9, 2, 'admin', '管理员', 'admin 登录了学之思考试系统', systimestamp);
INSERT INTO t_user_event_log VALUES (10, 2, 'admin', '管理员', 'admin 登出了学之思考试系统', systimestamp);
INSERT INTO t_user_event_log VALUES (11, 2, 'admin', '管理员', 'admin 登录了学之思考试系统', systimestamp);
INSERT INTO t_user_event_log VALUES (12, 2, 'admin', '管理员', 'admin 登出了学之思考试系统', systimestamp);
INSERT INTO t_user_event_log VALUES (13, 2, 'admin', '管理员', 'admin 登录了学之思考试系统', systimestamp);
INSERT INTO t_user_event_log VALUES (14, 2, 'admin', '管理员', 'admin 登出了学之思考试系统', systimestamp);
INSERT INTO t_user_event_log VALUES (15, 2, 'admin', '管理员', 'admin 登录了学之思考试系统', systimestamp);
INSERT INTO t_user_event_log VALUES (16, 2, 'admin', '管理员', 'admin 登出了学之思考试系统', systimestamp);
INSERT INTO t_user_event_log VALUES (17, 2, 'admin', '管理员', 'admin 登录了学之思考试系统', systimestamp);
INSERT INTO t_user_event_log VALUES (18, 2, 'admin', '管理员', 'admin 登录了学之思考试系统', systimestamp);
INSERT INTO t_user_event_log VALUES (19, 1, 'student', '学生', 'student 登录了学之思考试系统', systimestamp);
INSERT INTO t_user_event_log VALUES (20, 1, 'student', '学生', 'student 提交试卷：固定试卷 得分：3 耗时：8 秒', systimestamp);
INSERT INTO t_user_event_log VALUES (21, 2, 'admin', '管理员', 'admin 登录了学之思考试系统', systimestamp);
INSERT INTO t_user_event_log VALUES (22, 2, 'admin', '管理员', 'admin 登录了学之思考试系统', systimestamp);
INSERT INTO t_user_event_log VALUES (23, 2, 'admin', '管理员', 'admin 登出了学之思考试系统', systimestamp);
INSERT INTO t_user_event_log VALUES (24, 2, 'admin', '管理员', 'admin 登录了学之思考试系统', systimestamp);
INSERT INTO t_user_event_log VALUES (25, 2, 'admin', '管理员', 'admin 登录了学之思考试系统', systimestamp);
INSERT INTO t_user_event_log VALUES (26, 2, 'admin', '管理员', 'admin 登录了学之思考试系统', systimestamp);
INSERT INTO t_user_event_log VALUES (27, 2, 'admin', '管理员', 'admin 登录了学之思考试系统', systimestamp);
INSERT INTO t_user_event_log VALUES (28, 2, 'admin', '管理员', 'admin 登录了学之思考试系统', systimestamp);
INSERT INTO t_user_event_log VALUES (29, 2, 'admin', '管理员', 'admin 登出了学之思考试系统', systimestamp);
INSERT INTO t_user_event_log VALUES (30, 2, 'admin', '管理员', 'admin 登出了学之思考试系统', systimestamp);
INSERT INTO t_user_event_log VALUES (31, 2, 'admin', '管理员', 'admin 登录了学之思考试系统', systimestamp);
INSERT INTO t_user_event_log VALUES (32, 2, 'admin', '管理员', 'admin 登录了学之思考试系统', systimestamp);
INSERT INTO t_user_event_log VALUES (33, 2, 'admin', '管理员', 'admin 登出了学之思考试系统', systimestamp);
INSERT INTO t_user_event_log VALUES (34, 2, 'admin', '管理员', 'admin 登录了学之思考试系统', systimestamp);
INSERT INTO t_user_event_log VALUES (35, 2, 'admin', '管理员', 'admin 登录了学之思考试系统', systimestamp);


INSERT INTO t_text_content (id, content, create_time) VALUES (1, '{"titleContent":"<p class=\"ueditor-p\">单选题单选题单选题单选题</p>","analyze":"单选题","questionItemObjects":[{"prefix":"A","content":"单选题","score":null},{"prefix":"B","content":"单选题","score":null},{"prefix":"C","content":"单选题","score":null}],"correct":"B"}', systimestamp);
INSERT INTO t_text_content (id, content, create_time) VALUES (2, '{"titleContent":"填空题<span class=\"gapfilling-span 60e3a2c5-7d2e-4651-82f5-1a33818d3dba\">1</span>分为非<span class=\"gapfilling-span 5242aa1d-87b3-42be-b827-b3e5c38e6e31\">2</span>分为非","analyze":"gregr&nbsp;","questionItemObjects":[{"prefix":"1","content":"fdsf","score":20},{"prefix":"2","content":"gfd","score":20}],"correct":""}', systimestamp);
INSERT INTO t_text_content (id, content, create_time) VALUES (3, '[{"name":"一、题目测试","questionItems":[{"id":1,"itemOrder":1},{"id":2,"itemOrder":2}]}]', systimestamp);
INSERT INTO t_text_content (id, content, create_time) VALUES (4, '["few","greg"]', systimestamp);
INSERT INTO t_text_content (id, content, create_time) VALUES (5, '{"titleContent":"1+1=？","analyze":"略","questionItemObjects":[{"prefix":"A","content":"0","score":null},{"prefix":"B","content":"1","score":null},{"prefix":"C","content":"2","score":null},{"prefix":"D","content":"3","score":null}],"correct":"C"}', systimestamp);
INSERT INTO t_text_content (id, content, create_time) VALUES (6, '["qwqwq","qwqwqwq"]', systimestamp);
INSERT INTO t_text_content (id, content, create_time) VALUES (7, '{"titleContent":"测试","analyze":"略","questionItemObjects":[{"prefix":"A","content":"1","score":null},{"prefix":"B","content":"2","score":null},{"prefix":"C","content":"3","score":null},{"prefix":"D","content":"4","score":null}],"correct":"C"}', systimestamp);
INSERT INTO t_text_content (id, content, create_time) VALUES (8, '[{"name":"小试牛刀","questionItems":[{"id":4,"itemOrder":1},{"id":3,"itemOrder":2}]}]', systimestamp);
INSERT INTO t_text_content (id, content, create_time) VALUES (9, '{"titleContent":"333333333333333333333333","analyze":"33333333333","questionItemObjects":[{"prefix":"A","content":"3333333","score":null},{"prefix":"B","content":"333333","score":null},{"prefix":"C","content":"33333333333333","score":null},{"prefix":"D","content":"33333333333","score":null}],"correct":"C"}', systimestamp);
INSERT INTO t_text_content (id, content, create_time) VALUES (10, '{"titleContent":"<p>为什么要学蛇？</p>","analyze":"不会就选C","questionItemObjects":[{"prefix":"A","content":"1","score":null},{"prefix":"B","content":"2","score":null},{"prefix":"C","content":"3","score":null},{"prefix":"D","content":"4","score":null}],"correct":"C"}', systimestamp);
INSERT INTO t_text_content (id, content, create_time) VALUES (11, '{"titleContent":"666","analyze":"666","questionItemObjects":[{"prefix":"A","content":"1","score":null},{"prefix":"B","content":"2","score":null},{"prefix":"C","content":"3","score":null},{"prefix":"D","content":"4","score":null}],"correct":"C"}', systimestamp);
INSERT INTO t_text_content (id, content, create_time) VALUES (12, '{"titleContent":"666","analyze":"666","questionItemObjects":[{"prefix":"A","content":"1","score":null},{"prefix":"B","content":"2","score":null},{"prefix":"C","content":"3","score":null},{"prefix":"D","content":"4","score":null}],"correct":"C"}', systimestamp);